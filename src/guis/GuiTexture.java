package guis;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class GuiTexture {

    private int texture;
    private float xPos;
    private float yPos;
    private float width;
    private float height;
    private float rotation;

    public GuiTexture(int texture, float xPos, float yPos, float rotation, float width, float height) {
        this.texture = texture;
        this.xPos = xPos;
        this.yPos = yPos;
        this.rotation = rotation;
        this.width = width;
        this.height = height;
    }

    public int getTexture() {
        return texture;
    }

    public float getRotation() {
        return rotation;
    }

    public void setTexture(int texture) {
        this.texture = texture;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public void increaseRotation(float increase) {
        this.rotation += increase;
    }

    public float getxPos() {
        return xPos;
    }

    public void setxPos(float xPos) {
        this.xPos = xPos;
    }

    public float getyPos() {
        return yPos;
    }

    public void setyPos(float yPos) {
        this.yPos = yPos;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }
}
