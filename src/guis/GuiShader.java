package guis;

import org.lwjgl.util.vector.Matrix4f;

import shaders.ShaderProgram;

public class GuiShader extends ShaderProgram{
	
	private static final String VERTEX_FILE = "src/guis/guiVertexShader.glsl";
	private static final String FRAGMENT_FILE = "src/guis/guiFragmentShader.glsl";
	
	private int location_transformationMatrix;
	private int location_modelMatrix;
	private int location_rotationMatrix;

	public GuiShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}
	
	public void loadTransformation(Matrix4f matrix){
		super.loadMatrix(location_transformationMatrix, matrix);
	}

	public void loadModel(Matrix4f matrix) {
		super.loadMatrix(location_modelMatrix, matrix);
	}

	public void loadRotation(Matrix4f matrix) {
		super.loadMatrix(location_rotationMatrix, matrix);
	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = super.getUniformLocation("transformationMatrix");
		location_modelMatrix = super.getUniformLocation("modelMatrix");
		location_rotationMatrix = super.getUniformLocation("rotationMatrix");
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
	}
	
	
	

}
