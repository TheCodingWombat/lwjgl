package guis;

import models.RawModel;
import org.lwjgl.opengl.*;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import renderEngine.Loader;
import toolbox.Maths;

import java.util.List;

public class GuiRenderer {

    private final RawModel quad;
    private GuiShader shader;

    public GuiRenderer(Loader loader) {
        float[] positions = {-1, 1, -1, -1, 1, 1, 1, -1};

        quad = loader.loadToVAO(positions);
        shader = new GuiShader();
    }

    public void render(List<GuiTexture> guis) {
        shader.start();
        GL30.glBindVertexArray(quad.getVaoID());
        GL20.glEnableVertexAttribArray(0);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glDisable(GL11.GL_DEPTH_TEST);

        for (GuiTexture gui : guis) {
            GL13.glActiveTexture(GL13.GL_TEXTURE0);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, gui.getTexture());

            float width = gui.getWidth() / 2;
            float height = gui.getHeight() / 2;

            Matrix4f modelMatrix = Maths.createTransformationMatrix(
                    new Vector2f(
                            -1,
                            1
                    ), gui.getRotation()
                    ,new Vector2f(
                            2.0f / (float) Display.getWidth(),
                            2.0f / (float) Display.getHeight()
                    )
            );

            shader.loadModel(modelMatrix);

            Matrix4f transformationMatrix = Maths.createTransformationMatrix(
                    new Vector2f(
                               gui.getxPos() + width,
                            - (gui.getyPos() + height)
                    ), gui.getRotation()
                    ,new Vector2f(
                            width,
                            height
                    )
            );

            shader.loadTransformation(transformationMatrix);


            GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, quad.getVertexCount());
        }
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDisable(GL11.GL_BLEND);
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);
        shader.stop();
    }

    public void cleanUp() {
        shader.cleanUp();
    }
}
