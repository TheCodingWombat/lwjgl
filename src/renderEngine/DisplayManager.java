package renderEngine;

import org.lwjgl.Sys;
import org.lwjgl.opengl.*;
import org.lwjgl.LWJGLException;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by Richard on 23/10/2015.
 */
public class DisplayManager {

    private static final int WIDTH = 1600;
    private static final int HEIGHT = 900;
    private static final int FPS_CAP = 120;

    private static long lastFrameTime;
    private static float deltaTime;

    public static void createDisplay() {
        ContextAttribs attribs = new ContextAttribs(4,4).withForwardCompatible(true).withProfileCore(true);

        try {
            Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
            Display.create(new PixelFormat(), attribs);
            Display.setTitle("My first 3D game");
        } catch (LWJGLException e) {
            e.printStackTrace();
        }

        GL11.glViewport(0, 0, WIDTH, HEIGHT);
        lastFrameTime = getCurrentTime();
    }

    public static void updateDisplay(){
        Display.sync(FPS_CAP);
        Display.update();

        long currentFrameTime = getCurrentTime();
        deltaTime = (currentFrameTime - lastFrameTime) / 1000f;

        lastFrameTime = currentFrameTime;
    }

    public static float getFrameTimeSeconds() {
        return deltaTime;
    }

    public static void closeDisplay(){
        Display.destroy();
    }

    private static long getCurrentTime() {
        return Sys.getTime() * 1000 / Sys.getTimerResolution();
    }

}
