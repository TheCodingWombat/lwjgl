package engineTester;

import entities.Camera;
import entities.Entity;
import entities.Light;
import guis.GuiRenderer;
import guis.GuiTexture;
import models.RawModel;
import models.TexturedModel;
import objConverter.ModelData;
import objConverter.OBJFileLoader;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.*;
import textures.ModelTexture;

import java.util.ArrayList;
import java.util.List;

public class MainGameLoop {

    public static void main(String[] args) {
        DisplayManager.createDisplay();

        Loader loader = new Loader();

        ModelData modelData = OBJFileLoader.loadOBJ("stall");
        RawModel model = loader.loadToVAO(modelData.getVertices(), modelData.getTextureCoords(), modelData.getNormals(), modelData.getIndices());

        ModelTexture texture = new ModelTexture(loader.loadTexture("stallTexture"));
        texture.setShineDamper(10);
        texture.setReflectivity(0);

        TexturedModel texturedModel = new TexturedModel(model, texture);
        Entity entity = new Entity(texturedModel, new Vector3f(0, -3, -25), 0, 180, 0, 1);

        Camera camera = new Camera(new Vector3f(0, 0, 0));
        Light light = new Light(new Vector3f(0, 0, 20), new Vector3f(1, 1, 1));
        MasterRenderer renderer = new MasterRenderer();

        float aspectRatioPhone = 2.05f;

        List<GuiTexture> guis = new ArrayList<>();
        GuiTexture phone = new GuiTexture(loader.loadTexture("opaqueRect"), 500, 500, -15, 50, 50);
        guis.add(phone);

//        GuiTexture gui2 = new GuiTexture(loader.loadTexture("cannonBlueFull"), new Vector2f(0.6f, 0.6f), new Vector2f(0.25f, 0.25f));
//        guis.add(gui2);

        GuiRenderer guiRenderer = new GuiRenderer(loader);

        while(!Display.isCloseRequested()) {
            camera.move();


//            entity.increaseRotation(0, 0, .1f);

            renderer.processEntity(entity);
            renderer.render(light, camera);

            phone.increaseRotation(.05f);

            guiRenderer.render(guis);

            DisplayManager.updateDisplay();
        }

        guiRenderer.cleanUp();
        renderer.cleanUp();
        loader.cleanUp();
        DisplayManager.closeDisplay();

    }
}