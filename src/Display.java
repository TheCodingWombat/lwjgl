//import models.RawModel;
//import models.TexturedModel;
//import org.lwjgl.glfw.GLFWErrorCallback;
//import org.lwjgl.opengl.GL;
//import org.lwjgl.system.MemoryUtil;
//import shaders.StaticShader;
//import textures.ModelTexture;
//
//import static org.lwjgl.glfw.GLFW.*;
//import static org.lwjgl.opengl.GL11.*;
//
//public class Display {
//    private static GLFWErrorCallback errorCallback;
//    private static long windowID;
//
//    public static void main(String[] args) {
//        errorCallback = GLFWErrorCallback.createPrint(System.err);
//        glfwSetErrorCallback(errorCallback);
//
//        int glfwInitializationResult = glfwInit();
//        if (glfwInitializationResult == GL_FALSE)
//            throw new IllegalStateException("GLFW initialization failed");
//
//        windowID = glfwCreateWindow(
//                1600, 900,
//                "Display",
//                MemoryUtil.NULL,
//                MemoryUtil.NULL
//        );
//
//        if (windowID == MemoryUtil.NULL)
//            throw new IllegalStateException("GLFW window creation failed");
//
//        glfwMakeContextCurrent(windowID);
//        glfwSwapInterval(1);
//        glfwShowWindow(windowID);
//
//        GL.createCapabilities();
//
//        renderEngine.Loader loader = new renderEngine.Loader();
//        renderEngine.Renderer renderer = new renderEngine.Renderer();
//        StaticShader shader = new StaticShader();
//
//        float[] vertices = {
//                -0.5f, 0.5f, 0f,
//                -0.5f, -0.5f, 0f,
//                0.5f, -0.5f, 0f,
//                0.5f, 0.5f, 0f,
//        };
//
//        float[] colours = {
//                1f, 0, 1f,
//                0, 1f, 0,
//                1f, 0, 0,
//                1f, 0, 0
//        };
//
//        int[] indices = {
//                0, 1, 3,
//                3, 1, 2
//        };
//
//        RawModel model = loader.loadToVAO(vertices, colours, indices);
//        ModelTexture texture = new ModelTexture(loader.loadTexture("black64"));
//        TexturedModel texturedModel = new TexturedModel(model, texture);
//
//        while (glfwWindowShouldClose(windowID) == GL_FALSE) {
//            renderer.prepare();
//            shader.start();
//            renderer.render(texturedModel);
//            shader.stop();
//
//            glfwSwapBuffers(windowID);
//            glfwPollEvents();
//        }
//
//
//        shader.cleanUp();
//        loader.cleanUp();
//        glfwDestroyWindow(windowID);
//        glfwTerminate();
//    }
//}